export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="avit"
HIST_STAMPS="dd/mm/yyyy"
plugins=(git colorize common-aliases ssh-agent)

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/base16-solarized.dark.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

# Redirect vim to external one if exists
if [ -d /Applications/MacVim.app ]; then
    alias vim=/Applications/MacVim.app/Contents/MacOS/Vim
    alias gvim=/Applications/MacVim.app/Contents/MacOS/MacVim
fi

# Sublime
if [ -d /Applications/Sublime\ Text.app ]; then
    alias s="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
fi

source $ZSH/oh-my-zsh.sh

alias zz="source $HOME/.zshrc"
# Bower aliases
alias bi="bower install"
alias bis="bower install --save"
alias bid="bower install --save-dev"
# Npm aliases
alias ni="npm install"
alias nis="npm install --save"
alias nid="npm install --save-dev"
alias nig="npm install -g"
# Install dependencies
alias I="npm install && bower install"
# Unalias
unalias rm

if [[ -f $HOME/local.sh ]]; then
    source $HOME/local.sh
fi

# added by travis gem
[ -f /Users/marioagostinolamacchia/.travis/travis.sh ] && source /Users/marioagostinolamacchia/.travis/travis.sh

# PATH
export PATH=$HOME/bin:/usr/local/bin:$HOME/.node/bin:$PATH:$HOME/.composer/vendor/bin
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="$PATH:$HOME/Library/Python/2.7/bin" # Add user pip to path
export PATH="/usr/local/sbin:$PATH"
export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

# GEMS
export GEM_HOME="$HOME/.gem"
export PATH="$PATH:$HOME/.gem/bin"
[[ -s "$HOME/.avn/bin/avn.sh" ]] && source "$HOME/.avn/bin/avn.sh" # load avn
