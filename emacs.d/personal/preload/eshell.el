# Modules
Here are all modules not linked to dynamic database collections. For each
module are also listed all the belonging apps.

## sample

- list: Used to show list of documents
- detail: Used to show and edit a single document

## setting

- change_password: Changes user's password
- company: Show company's detail
- create_app: Creates app
- template_lsit: Lists template apps
- user: Show an user's details
- user_list: Show all company's users

# Data Modules
Every module in this section is linked to a dynamic collection into the
database. For each module there is the list of the belonging apps and, for each
app, there is the list of the fields in the collection.

## accounting

- customer (general.contact)
- seller (general.contact)

## general

- contact
    - name: String
    - tax_code: String
    - address: Address
    - phones: Subdocument
        - phone: String
    - emails: Subdocument
        - email: String

## shipping

- cmr
    - transport_document: String
    - date: Date
    - carriage: Enum (freeport, carriage_forward, other)
    - goods_value: Currency
    - goods_weight: Currency
    - goods_volume: Currency
    - fixed_fee: Currency
    - withdrawal_charges: Currency
    - delivery_charges: Currency
    - mark_charges: Currency
    - insurance_charges: Currency
    - mark: Boolean
    - insurance: Boolean
    - freights: Subdocument
    - discouts_and_charges: Subdocument
- driver (general.contact)
- vehicle
    - type: String
    - tag: String
- waybill
    - date: Date
    - vehicle: -> shipping.vehicle
    - drivers: Subdocument
        - driver: -> shipping.drive
    - departure: Datetime
    - arrival: Datetime
    - shipping_cost: Currency
    - loading: String
    - unloading: String
    - cmrs: Subdocument
        - cmr -> shipping.cmr

## store

- group {Tree}
    - name: String
    - description: Text
- item
    - name: String
    - code: String
    - bar_code: String
    - group: -> store.group
