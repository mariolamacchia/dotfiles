### MacOSX ###
echo "Current os: Darwin"

# brew dependencies
echo "Installing brew dependencies..."
brew install wget node nvm
sudo easy_install pip

# install iTerm2
if [[ ! -d /Application/iTerm.app ]]; then
    echo "Installing iTerm2..."
    curl https://iterm2.com/downloads/stable/iTerm2-3_0_12.zip > iTerm.zip
    unzip iTerm.zip
    mv iTerm.app /Application
    rm iTerm.zip
    echo "iTerm2 installed. Remember to load preferences from $HOME/dotfiles/item2/"
fi
