#!/bin/bash

##### Install os programs #####
zsh install/os.sh
zsh install/zsh.sh

##### Install #####
home=~
dir=~/dotfiles
backupdir="$home/.olddotfiles/$(date +%s)"
files="zshrc noderc gitignore gitconfig vim vimrc vimshrc atom"

# Create backup dir
if [ ! -d "$backupdir" ]; then
    echo "Old configurations in $backupdir"
    mkdir -p "$backupdir"
fi
# Create config dir
if [ ! -d "$home/.config" ]; then
    mkdir "$home/.config"
fi;

##### FONTS #####
if grep -q powerline "$dir/.install"; then
    git clone https://github.com/Lokaltog/powerline-fonts
    ./powerline-fonts/install.sh
    rm -rf powerline-fonts
    echo powerline >> "$dir/.install"
fi

##### ZSH #####
# Clone oh-my-zsh
if [ ! -d "$home/.oh-my-zsh" ]; then
    echo "Cloning oh-my-zsh..."
    git clone https://github.com/robbyrussell/oh-my-zsh "$home/.oh-my-zsh"
fi

# Custom oh-my-zsh
# ernola theme
if [ -L "$home/.oh-my-zsh/themes/ernola.zsh-theme" ]; then
    unlink "$home/.oh-my-zsh/themes/ernola.zsh-theme"
fi
echo "Installing ernola theme..."
ln -s "$dir/oh-my-zsh/themes/ernola.zsh-theme" \
    "$home/.oh-my-zsh/themes/ernola.zsh-theme"
# solarized
if [ ! -d "$home/.config/base16-shell" ]; then
    echo "Cloning base16-shell..."
    git clone https://github.com/chriskempson/base16-shell \
        "$home/.config/base16-shell"
fi

##### EMACS #####
# Install Emacs Prelude
if [ -d "$home/.emacs.d" ]; then
    echo "Saving old Emacs settings"
    mv "$home/.emacs.d" "$backupdir"
fi
if [ ! -d "$home/.emacs-prelude" ]; then
    echo "Cloning Emacs Prelude..."
    git clone git://github.com/bbatsov/prelude.git "$home/.emacs-prelude"
fi
if [ ! -L "$home/.emacs.d" ]; then
    echo "Linking Emacs Prelude..."
    ln -s "$home/.emacs-prelude" "$home/.emacs.d"
fi
# Add personal configurations
echo "Adding personal configurations to emacs..."
mkdir -p "$backupdir/emacs.d/personal/preload"
mv "$home/.emacs.d/personal/preload/"* "$backupdir/emacs.d/personal/preload"
for file in $(ls "$dir/emacs.d/personal/preload"); do
    echo "|- $file"
    ln -s "$dir/emacs.d/personal/preload/$file" \
        "$home/.emacs.d/personal/preload/$file"
done

##### VIM #####
if [ ! -d "$dir/vim" ]; then
    echo "Creating vim dir..."
    mkdir -p "$dir/vim/bundle"
fi
if [ ! -d "$dir/vim/bundle/Vundle.vim" ]; then
    echo "Cloning Vundle..."
    git clone git://github.com/gmarik/Vundle.vim.git "$dir/vim/bundle/Vundle.vim"
fi

##### SUBLIME #####
if [ -d "$home/Library/Application Support/Sublime Text 3/Packages/User" ]; then
    echo "Linking Sublime Text 3 configurations (remember to install packagecontrol)..."
    mv "$home/Library/Application Support/Sublime Text 3/Packages/User" $backupdir
    ln -s "$dir/sublime" "$home/Library/Application Support/Sublime Text 3/Packages/User"
else
    echo "Sublime not found"
fi

##### SYMLINKS #####
for file in $files; do
    echo "Creating symlink to $file in ~"
    if [ -L "$home/.$file" ]; then
        unlink "$home/.$file"
    elif [ -d "$home/.$file" ] || [ -f "$home/.$file" ]; then
        mv "$home/.$file" "$backupdir/$file"
    fi
    ln -s "$dir/$file" "$home/.$file"
done

##### VIM PLUGINS #####
read -p "Shell I install and configure VIM? (y/N): " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    vim +PluginInstall +qall
    # build YouCompleteMe !!! Requires CMake !!!
    read -p "Shell I build YouCompleteMe? (y/N): " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        echo # newline
        echo "Building YouCompleteMe"
        cd "$home/.vim/bundle/YouCompleteMe"
        ./install.sh --clang-completer
    fi
    # Ctags
    if [[ ! -f "$home/.vim/ctags/ctags" ]]; then
        read -p "Shell I download and build ctags? (Y/n): " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^[Nn]$ ]]; then
            echo # newline
            cd "$home/.vim"
            wget -O ctags.tar.gz "http://sourceforge.net/projects/ctags/files/ctags/5.8/ctags-5.8.tar.gz/download?use_mirror=softlayer-ams"
            tar -xf ctags.tar.gz
            mv ctags-5.8 ctags
            cd ctags
            ./configure
            make
            rf -f ctags.tar.gz
        fi
    fi
    # tern
    echo "Installing vim dependencies..."
    cd "$home/.vim/bundle/tern_for_vim"
    npm install

    ##### ECLIM #####
    read -p "Shell I install eclim? (y/N): " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        eclipse=$(dirname $(readlink `which eclipse`))
        wget http://sourceforge.net/projects/eclim/files/latest/download?source=typ_redirect -O "$home/eclim.jar"
        java -jar "$home/eclim.jar"
        rm -f "$home/eclim.jar"
        ln -s $eclipse/eclimd /usr/local/bin
    fi
fi

##### ATOM #####
read -p "Shell I install atom dependencies? (y/N): " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    cat "$dir/atom/packages.txt" | xargs apm install
fi
