set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Autocomplete
Plugin 'Valloric/YouCompleteMe'
" Plugin 'Sirver/ultisnips'
" Plugin 'honza/vim-snippets'
" Plugin 'Raimondi/delimitMate'

" Git
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" Syntax
Plugin 'scrooloose/syntastic'
Plugin 'bronson/vim-trailing-whitespace'

" Basic
Plugin 'elzr/vim-json'

" Html
Plugin 'othree/html5.vim'
Plugin 'mattn/emmet-vim'

" CSS3
Plugin 'hail2u/vim-css3-syntax'
Plugin 'tpope/vim-haml'
Plugin 'wavded/vim-stylus'

" JavaScript
Plugin 'pangloss/vim-javascript'
Plugin 'kchmck/vim-coffee-script'
Plugin 'nono/jquery.vim'
Plugin 'othree/javascript-libraries-syntax.vim'
Plugin 'burnettk/vim-angular'
Plugin 'leafgarland/typescript-vim'
Plugin 'moll/vim-node'
Plugin 'marijnh/tern_for_vim'

" PHP
Plugin 'StanAngeloff/php.vim'

" Python
Plugin 'davidhalter/jedi-vim'
Plugin 'nvie/vim-flake8'

" Templating
Plugin 'digitaltoad/vim-jade'
Plugin 'evidens/vim-twig'
Plugin 'mustache/vim-mustache-handlebars'

" Others
Plugin 'kennethzfeng/vim-raml'

" Theme
Plugin 'chriskempson/base16-vim'

" Interface
Plugin 'Shougo/unite.vim'
Plugin 'majutsushi/tagbar'
Plugin 'bling/vim-airline'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'dbakker/vim-projectroot'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Apply solarized theme
syntax enable
set background=dark
colorscheme base16-solarized
" DelimitMate
let g:delimitMate_expand_cr = 2
" CtrlP
let g:ctrlp_root_markers = ['.classpath']
set wildignore+=*/tmp/*,*/node_modules/*,*/bower_components/*,*.o,*.png,*.jpg,*.zip,*.tar,*.pyc,*.min.js
let g:ctrlp_user_command =
  \ ['.git', 'cd %s && git ls-files . -co --exclude-standard']
nmap <C-p> :CtrlPMixed<CR>
" YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
" UltiSnips
let g:UltiSnipsExpandTrigger='<c-return>'
let g:UltiSnipsJumpForwardTrigger='<c-return>'
let g:UltiSnipsJumpBackwardTrigger='<s-return>'
" Tagbar
nmap <F8> :TagbarToggle<CR>
let g:tagbar_ctags_bin = '~/.vim/ctags/ctags'
" Vim Indent Guide
let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_guide_size=1
" Vim airline
set guifont=Source\ Code\ Pro\ for\ Powerline
let g:airline_powerline_fonts = 1
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme_patch_func = 'SolarizedPatch'
function! SolarizedPatch(palette)
    if g:airline_theme == 'solarized'
        let a:palette.normal['airline_a'][1] = '#2aa198'
    endif
endfunction
" VimShell
let g:vimshell_user_prompt = 'getcwd()'
let g:vimshell_prompt = '$ '

""""" Other configurations """""
" Case insensitive search
set ignorecase
" Tabs
set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4
" Row length
set cc=80
" Row numbers
set nu
" GUI
set guioptions-=m  " remove menu bar
set guioptions-=T  " remove toolbar
set guioptions-=L  " remove left-hand scroll bar
" Show typed commands
set showcmd
" Search
set incsearch
set hlsearch
" Auto complete bash mode
set wildmode=longest,list,full
set wildmenu
" Backspace
set backspace=indent,eol,start

""""" Mappings """""
" Remap leader
let mapleader=","

" Disable man page
noremap K <NOP>

" Tabs
nnoremap <Leader>t :tabnew<CR>
nnoremap <Leader>d :tabclose<CR>
nnoremap gh :tabfirst<CR>
nnoremap gk :tabnext<CR>
nnoremap gj :tabprevious<CR>
nnoremap gl :tablast<CR>
nnoremap >t :tabmove +1<CR>
nnoremap <t :tabmove -1<CR>

" Windows
nmap <silent> <C-W><A-+> :resize +1<CR><SID>ws
nmap <silent> <C-W><A--> :resize -1<CR><SID>ws
nmap <silent> <C-W>+ :vertical resize +1<CR><SID>ws
nmap <silent> <C-W>- :vertical resize -1<CR><SID>ws
nmap <silent> <C-W><A-*> :resize +5<CR><SID>ws
nmap <silent> <C-W><A-_> :resize -5<CR><SID>ws
nmap <silent> <C-W>* :vertical resize +5<CR><SID>ws
nmap <silent> <C-W>_ :vertical resize -5<CR><SID>ws
nn <script> <SID>ws<A-+> <C-W>+<SID>ws
nn <script> <SID>ws<A--> <C-W>-<SID>ws
nn <script> <SID>ws+ <C-W>><SID>ws
nn <script> <SID>ws- <C-W><<SID>ws
nn <script> <SID>ws* 5<C-W>><SID>ws
nn <script> <SID>ws_ 5<C-W><<SID>ws
nn <script> <SID>ws<A-*> 5<C-W>+<SID>ws
nn <script> <SID>ws<A-_> 5<C-W>-<SID>ws
nmap <SID>ws <Nop>
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
nnoremap <C-w>d :q<CR>

" Open files
nnoremap <C-p> :Unite file_rec/git<CR>i

" NERDTree
map <C-x> <plug>NERDTreeTabsToggle<CR>
let g:NERDTreeMapOpenInTabSilent = '<2-LeftMouse>'
